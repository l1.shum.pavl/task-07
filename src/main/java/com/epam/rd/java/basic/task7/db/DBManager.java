package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static final String SQL_FIND_ALL_USERS = "select * from users";
    private static final String SQL_INSERT_USER = "insert into users (login) values (?)";
    private static final String SQL_INSERT_TEAM = "insert into teams (name) values (?)";
    private static final String SQL_FIND_ALL_TEAMS = "select * from teams";
    private static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE name=?";
    private static final String SQL_DELETE_USERS = "DELETE FROM users WHERE login=?";
    private static final String SQL_GET_USER = "select * from users  where login = ?";
    private static final String SQL_GET_TEAM = "select * from teams  where name = ?";
    private static final String SQL_SET_TEAMS_FOR_USER = "insert into users_teams (user_id, team_id) values (?, ?)";
    private static final String SQL_GET_USER_TEAMS = "select teams.name from users_teams , teams where user_id =? and team_id = teams.id";


    private static String getURL() {
        Properties properties = new Properties();
        try (InputStream in = Files.newInputStream(Paths.get("app.properties"))) {
            properties.load(in);
        } catch (IOException exception) {
            System.err.println("Cannot find file.");
        }
        return properties.getProperty("connection.url");
    }

    private static Connection getConnection(String connectionUrl) throws DBException {
        try {
            return DriverManager.getConnection(connectionUrl);
        } catch (SQLException throwables) {
            throw new DBException("err", throwables);
        }
    }

    private static final DBManager instance = new DBManager();

    public static synchronized DBManager getInstance() {
        return instance;
    }


    private DBManager() {
//        try {
//            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
//            System.out.println("Connection succesfull!");
//        } catch (Exception ex) {
//            System.out.println("Connection failed...");
//            System.out.println(ex);
//        }
    }


    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList();
        try (Connection conn = getConnection(getURL())) {
            System.out.println("find all users");

            PreparedStatement preparedStatement = conn.prepareStatement(SQL_FIND_ALL_USERS);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User user = this.getUser(resultSet.getString("login"));
                users.add(user);
            }

        } catch (SQLException throwables) {
            throw new DBException("some", throwables);
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        try (Connection conn = getConnection(getURL())) {
            System.out.println("insert user");

            PreparedStatement preparedStatement = conn.prepareStatement(SQL_INSERT_USER);
            preparedStatement.setString(1, user.getLogin());
            if (preparedStatement.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException throwables) {
            throw new DBException("some", throwables);
        }
        return false;
    }

    public boolean deleteUsers(User... users) throws DBException {
        boolean done = false;
        try (Connection conn = getConnection(getURL())) {
            System.out.println("delete users");
            for (int i = 0; i < users.length; i++) {
                PreparedStatement preparedStatement = conn.prepareStatement(SQL_DELETE_USERS);
                preparedStatement.setString(1, users[i].getLogin());
                if (preparedStatement.executeUpdate() > 0) done = true;
            }
        } catch (SQLException throwables) {
            throw new DBException("some", throwables);
        }
        return done;
    }

    public User getUser(String login) throws DBException {
        User user = null;
        try (Connection conn = getConnection(getURL())) {
            System.out.println("get user");

            PreparedStatement preparedStatement = conn.prepareStatement(SQL_GET_USER);
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user = User.createUser(login);
                user.setId(resultSet.getInt("id"));
            }

        } catch (SQLException throwables) {
            throw new DBException("some", throwables);
        }
//        System.out.println(user);
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;
        try (Connection conn = getConnection(getURL())) {
            System.out.println("get team");

            PreparedStatement preparedStatement = conn.prepareStatement(SQL_GET_TEAM);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                team = Team.createTeam(name);
                team.setId(resultSet.getInt("id"));
            }
        } catch (SQLException throwables) {
            throw new DBException("some", throwables);
        }
        System.out.println(team.getId());
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList();
        try (Connection conn = getConnection(getURL())) {
            System.out.println("find all teams");
            PreparedStatement preparedStatement = conn.prepareStatement(SQL_FIND_ALL_TEAMS);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Team team = getTeam(resultSet.getString("name"));
                team.setId(resultSet.getInt("id"));
                teams.add(team);
            }
        } catch (SQLException throwables) {
            throw new DBException("some", throwables);
        }

        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection conn = getConnection(getURL())) {
            System.out.println("insert team");

            PreparedStatement preparedStatement = conn.prepareStatement(SQL_INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, team.getName());

            if (preparedStatement.executeUpdate() > 0) {
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    team.setId(resultSet.getInt(1));
                    System.out.println("from generated keys " + team + " " + team.getId());
                }
                return true;
            }
        } catch (SQLException throwables) {
            throw new DBException("some", throwables);
        }
        return false;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        boolean done = false;
        Connection con = getConnection(getURL());
        try {
            con.setAutoCommit(false);

            Savepoint save = con.setSavepoint("Savepoint1");
            try {
                int userId = getUser(user.getLogin()).getId();

                System.out.printf("set teams for user %s \n", user);


                for (int i = 0; i < teams.length; i++) {
                    int teamId = getTeam(teams[i].getName()).getId();
                    System.out.println("team id " + teamId);
                    PreparedStatement preparedStatement = con.prepareStatement(SQL_SET_TEAMS_FOR_USER);
                    preparedStatement.setInt(1, userId);
                    preparedStatement.setInt(2, teamId);
                    preparedStatement.executeUpdate();
                }


                con.commit();
                System.out.println("near commit");
                done = true;
            } catch (SQLException e) {
                try {
                    System.out.println("near rollback");
                    con.rollback(save);
                    done = false;
                } catch (SQLException e2) {
                    throw new DBException("some", e2);
                }
                throw new DBException("some", e);
            }

        } catch (SQLException throwables) {
            throw new DBException("some", throwables);

        } finally {
            try {
                con.setAutoCommit(true);
                con.close();
                System.out.println("con close");
            } catch (SQLException e) {
                throw new DBException("some", e);
            }
        }
        return done;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList();
        try (Connection conn = getConnection(getURL())) {
            System.out.println("get user teams");

            int userID = getUser(user.getLogin()).getId();

            PreparedStatement statement = conn.prepareStatement(SQL_GET_USER_TEAMS);
            statement.setInt(1, userID);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Team team = getTeam(resultSet.getString("name"));
                teams.add(team);
            }

        } catch (SQLException throwables) {
            throw new DBException("get user teams ", throwables);
        }

//        Comparator<Team> tc = new Comparator<Team>() {
//            @Override
//            public int compare(Team o1, Team o2) {
//                return o1.getName().compareTo(o2.getName());
//            }
//        };
//        teams.sort(tc);
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (Connection conn = getConnection(getURL())) {
            System.out.println("delete team");

            PreparedStatement preparedStatement = conn.prepareStatement(SQL_DELETE_TEAM);
            preparedStatement.setString(1, team.getName());
            if (preparedStatement.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            throw new DBException("some", throwables);
        }
        return false;
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection conn = getConnection(getURL())) {
            System.out.println("update team");
            System.out.println(team.getName() + "  " + team.getId());
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "update teams " +
                            "set name = ?" +
                            "where id = ?");

            preparedStatement.setString(1, team.getName());
            preparedStatement.setInt(2, team.getId());

            if (preparedStatement.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException throwables) {
            throw new DBException("some", throwables);
        }
        return false;
    }


}
