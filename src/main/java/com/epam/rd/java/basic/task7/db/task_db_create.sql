-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema task_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema task_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `task_db` DEFAULT CHARACTER SET utf8 ;
USE `task_db` ;

-- -----------------------------------------------------
-- Table `task_db`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `task_db`.`users` (
  id INT  NOT NULL auto_increment,
  `login` VARCHAR(45) unique,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `task_db`.`teams`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `task_db`.`teams` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `task_db`.`users_teams`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `task_db`.`users_teams` (
  `users_id` INT ,
  `teams_id` INT ,
  unique (`users_id`, `teams_id`),
  CONSTRAINT `fk_users_teams_users`
    FOREIGN KEY (`users_id`)
    REFERENCES `task_db`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_has_teams_teams`
    FOREIGN KEY (`teams_id`)
    REFERENCES `task_db`.`teams` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

insert into users (id, login) values (1, "ivanov");
insert into teams (id, name) values (1, "teamA");

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
